import pandas as pd

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-chipseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-chipseq-fastq/"
metadata = pd.read_csv(WORKING_DIR + "march2021_metadata.csv", sep=";")

# introduce a check that checks whehter metadata behaves well....

rule all:
  input:
    expand(FASTQ_DIR + "{e}.fq.gz", e=metadata.SAMPLE.tolist())

rule get_data:
  output:
      FASTQ_DIR + "{e}.fq.gz"
  params:
      e = lambda wildcards: {wildcards.e},
      srr = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].SRR.values[0],
      is_not_local = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].LOCAL_PATH.isnull().values[0],
      is_not_ftp = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].FASTQ_FTP_PREFIX.isnull().values[0],
      ftp_prefix = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].FASTQ_FTP_PREFIX.values[0],
      local_path = lambda wildcards: metadata[metadata.SAMPLE == wildcards.e].LOCAL_PATH.values[0],
      fq_dir = FASTQ_DIR
  run:
      if params.is_not_ftp:
          shell("cp {params.local_path}.fastq.gz {params.fq_dir}{params.e}.fq.gz")
      elif params.is_not_local:
          shell("rm -f {params.fq_dir}{params.srr}.fastq.gz && wget {params.ftp_prefix}.fastq.gz -P {params.fq_dir} && mv {params.fq_dir}{params.srr}.fastq.gz {params.fq_dir}{params.e}.fq.gz")
      else:
          error("The sample is neither local nor has a ftp link.")
