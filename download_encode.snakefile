import pandas as pd

FASTQ_DIR = "/fast/users/helmsauk_c/scratch/l1cam-chipseq-fastq/"
WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-chipseq/"
metadata = pd.read_csv(WORKING_DIR + "encode_metadata.csv", sep=";")

# introduce a check that checks whehter metadata behaves well....

rule all:
  input:
    expand(FASTQ_DIR + "{e}.fq.gz", e=metadata.FILE.tolist())

rule get_data:
  output:
      FASTQ_DIR + "{e}.fq.gz"
  params:
      e = lambda wildcards: {wildcards.e},
      file_accession = lambda wildcards: metadata[metadata.FILE == wildcards.e].ENCODE_FILE_ACCESSION.values[0],
      fastq_link = lambda wildcards: metadata[metadata.FILE == wildcards.e].ENCODE_FASTQ_FILE.values[0],
      fq_dir = FASTQ_DIR
  shell:
      "cd {params.fq_dir} && wget {params.fastq_link} && mv {params.file_accession}.fastq.gz {params.e}.fq.gz"

# rule cat_data:
#     input:
#         expand(FASTQ_DIR + "{f}.fq.gz", e=metadata.SAMPLE_FILE.tolist())
#     output:
#         expand(FASTQ_DIR + "{e}.fq.gz", e=metadata.SAMPLE.tolist())
#     run:
#         for s in
