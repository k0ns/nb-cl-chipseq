import pandas as pd

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-chipseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/l1cam-chipseq-fastq/"
metadata = pd.read_csv(WORKING_DIR + "sra_metadata.csv", sep=";")

rule all:
  input:
    expand(FASTQ_DIR + "{e}.fq.gz", e=metadata.SAMPLE.tolist())

rule download:
    output:
        FASTQ_DIR + "{srr}.fastq"
    params:
        srr = "{srr}"
    conda:
        "sra.yaml"
    shell:
        "fasterq-dump --outfile {output} {params.srr}"

rule join_files:
    input:
        lambda wildcards: [FASTQ_DIR + "{srr}.fastq".format(srr=row.SRR) for index, row in metadata[metadata.SAMPLE == wildcards.sample].iterrows()]
    output:
        FASTQ_DIR + "{sample}.fq.gz"
    params:
        srr = lambda wildcards: metadata[metadata.SAMPLE == wildcards.sample].SRR.values[0]
    shell:
        "cat {input} | gzip -c > {output} && rm -rf {input}"
