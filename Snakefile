import pandas as pd

WORKING_DIR = "/fast/users/helmsauk_c/work/nb-cl-chipseq/"
FASTQ_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-chipseq-fastq/"
BAM_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-chipseq/bam/"
MACS2_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-chipseq/MACS2/"
MACS2_BROAD_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-chipseq/MACS2_broad/"
HMCAN_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-chipseq/hmcan/"
LILY_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-chipseq/LILY/"
SVABA_DIR = "/fast/users/helmsauk_c/scratch/nb-cl-chipseq/svaba/"

HMCAN_BIN = "/fast/users/helmsauk_c/work/Applications/HMCan/src/HMCan"
HMCAN_CONFIG = WORKING_DIR + "hmcan-configs/HMCan.config.narrow.txt"
LILY_SCRIPT = "/fast/users/helmsauk_c/work/Applications/LILY/scripts/runLILY.R"
LILY_GENEANNOTATIONS = "/fast/users/helmsauk_c/work/Applications/rose2/rose2/annotation/hg19_refseq.ucsc"
LILY_FAI_FILE = "/fast/users/helmsauk_c/work/resources/hg19_bwa/hg19.fa.fai"

REFERENCE_PREINDEXED = "/fast/users/helmsauk_c/work/resources/hg19_bwa/hg19.fa"

BBMAP_DIR = "/fast/users/helmsauk_c/work/miniconda/envs/qc/opt/bbmap-38.58-0/"
ADAPTER_SEQUENCES = BBMAP_DIR + "resources/adapters.fa"
PICARD_JAR = '/fast/users/helmsauk_c/work/Applications/picard-2.20.4/picard.jar'
TMP_DIR = "/fast/users/helmsauk_c/scratch/tmp"
SAMTOOLS_PATH = "/fast/users/helmsauk_c/work/miniconda/bin/samtools"
SVABA_BIN_PATH = "/fast/users/helmsauk_c/work/svaba/bin/"

# mkdir /fast/users/helmsauk_c/work/resources/blacklists/ && cd /fast/users/helmsauk_c/work/resources/blacklists/
# wget http://hgdownload.cse.ucsc.edu/goldenPath/hg19/encodeDCC/wgEncodeMapability/wgEncodeDacMapabilityConsensusExcludable.bed.gz
# gunzip wgEncodeDacMapabilityConsensusExcludable.bed.gz
# bedtools merge -i wgEncodeDacMapabilityConsensusExcludable.bed > wgEncodeDacMapabilityConsensusExcludable.mergeBed.bed
MAPPABILITY_BLACKLIST = "/fast/users/helmsauk_c/work/resources/blacklists/wgEncodeDacMapabilityConsensusExcludable.mergeBed.bed"

# metadata = pd.read_csv(WORKING_DIR + "metadata.csv", sep=";", comment="#")
# encode_metadata = pd.read_csv(WORKING_DIR + "encode_metadata.csv", sep=";", comment="#")
# encode_metadata = encode_metadata[['SAMPLE', 'CONTROL']].drop_duplicates()
# sra_metadata = pd.read_csv(WORKING_DIR + "sra_metadata.csv", sep=";", comment="#")
# sra_metadata = sra_metadata[['SAMPLE', 'CONTROL']].drop_duplicates()
# metadata = pd.concat([metadata, encode_metadata, sra_metadata])

metadata = pd.read_csv(WORKING_DIR + "march2021_metadata.csv", sep=";", comment="#")
metadata_withcontrol = metadata[metadata.CONTROL.notnull()]
EXP = metadata.SAMPLE.tolist()
TREATMENT_TO_CONTROL = pd.Series(metadata_withcontrol.CONTROL.values,index=metadata_withcontrol.SAMPLE).to_dict()
TREATMENTS = list(TREATMENT_TO_CONTROL.keys())

rule all:
    input:
        expand(FASTQ_DIR + "{exp}_fastqc.html", exp=EXP),
        expand(FASTQ_DIR + "{exp}.trimmed_fastqc.html", exp=EXP),
        FASTQ_DIR + "multiqc_report.html",
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.bam.qc.txt", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.raw.bw", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.filtered.bw", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.bam.qc.txt", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.raw.bw", exp=EXP),
	    expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.bw", exp=EXP),
        expand(MACS2_DIR + "{treatment}/{treatment}_MACS2_peaks.narrowPeak", treatment=TREATMENTS),
        expand(MACS2_BROAD_DIR + "{treatment}/{treatment}_MACS2_broad_peaks.broadPeak", treatment=TREATMENTS),
        expand(MACS2_DIR + "{exp}/{exp}_MACS2_nocontrol_peaks.narrowPeak", exp=EXP),
        expand(MACS2_BROAD_DIR + "{exp}/{exp}_MACS2_nocontrol_broad_peaks.broadPeak", exp=EXP),
        expand(HMCAN_DIR + "{treatment}/{treatment}_hmcan_peaks.narrowPeak", treatment=TREATMENTS),
        expand(HMCAN_DIR + "{treatment}/{treatment}_hmcan_peaks.narrowPeak.mappability_filtered.bed", treatment=TREATMENTS),
        expand(LILY_DIR + "{treatment}_hmcan.scores.bed", treatment=TREATMENTS),
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.housekeeping_deeptools.png", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.allgenes_deeptools.png", exp=EXP),
        expand(SVABA_DIR + "{exp}.trimmed.bwa_hg19.rmdup.svaba.sv.vcf.gz", exp=EXP)

rule fastqc:
    input:
        "{exp}.fq.gz"
    output:
        "{exp}_fastqc.html"
    conda:
        WORKING_DIR + "envs/qc.yaml"
    shell:
        "fastqc {input}"

rule adapter_trimming:
    input:
        FASTQ_DIR + "{exp}.fq.gz"
    output:
        FASTQ_DIR + "{exp}.trimmed.fq.gz"
    conda:
        WORKING_DIR + "envs/qc.yaml"
    shell:
        BBMAP_DIR + "bbduk.sh in={input} out={output} ktrim=r k=23 mink=11 hdist=1 ref=" + ADAPTER_SEQUENCES + " tbo qtrim=r trimq=15 maq=20"

rule alignment:
    input:
        ref = REFERENCE_PREINDEXED,
        fastq = FASTQ_DIR + "{exp}.trimmed.fq.gz"
    output:
        bam = BAM_DIR + "{exp}.trimmed.bwa_hg19.bam"
    conda:
        WORKING_DIR + "envs/bwa.yaml"
    params:
        threads = "10",
        samtools_bin = SAMTOOLS_PATH
    shell:
        "bwa mem -M -t {params.threads} {input.ref} {input.fastq} | {params.samtools_bin} sort -@{params.threads} -o {output.bam} - && {params.samtools_bin} index {output.bam} && {params.samtools_bin} stats {output.bam} > {output.bam}.stats.txt"

rule remove_duplicates:
    input:
        bam = BAM_DIR + "{exp}.trimmed.bwa_hg19.bam"
    output:
        bam_markdup = BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.bam",
        bam_markdup_metrics = BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup-metrics.txt"
    params:
        tmp_dir = TMP_DIR,
        picard_jar = PICARD_JAR
    conda:
        WORKING_DIR + "envs/arima-mapping.yaml"
    shell:
        "java -Xmx30G -XX:-UseGCOverheadLimit -Djava.io.tmpdir={params.tmp_dir} -jar {params.picard_jar} MarkDuplicates INPUT={input.bam} OUTPUT={output.bam_markdup} METRICS_FILE={output.bam_markdup_metrics} TMP_DIR={params.tmp_dir} REMOVE_DUPLICATES=TRUE MAX_FILE_HANDLES_FOR_READ_ENDS_MAP=1000 CREATE_INDEX=TRUE"

rule normed_bigWig:
    input:
        "{bam_fname}.bam"
    output:
        "{bam_fname}.filtered.bw"
    params:
        blacklist = MAPPABILITY_BLACKLIST
    conda:
        WORKING_DIR + "envs/deeptools.yaml"
    shell:
        "bamCoverage --bam {input} -o {output} --binSize 10 --extendReads 200 --blackListFileName {params.blacklist} --normalizeUsing CPM -ignore chrM"

rule raw_bigWig:
    input:
        "{bam_fname}.bam"
    output:
        both_strands = "{bam_fname}.raw.bw",
        fwd_strand = "{bam_fname}.raw.fwd_strand.bw",
        rev_strand = "{bam_fname}.raw.rev_strand.bw"
    conda:
        WORKING_DIR + "envs/deeptools.yaml"
    shell:
        "bamCoverage --bam {input} -o {output.both_strands} && bamCoverage -b {input} -o {output.fwd_strand} --samFlagExclude 16 && bamCoverage -b {input} -o {output.rev_strand} --samFlagInclude 16"

# rule bigWigSummary:
#     input:
#         expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.bw", exp=EXP)
#     output:
#         BAM_DIR + "multiBigWigSummary.tab"
#     params:
#         all_bigwigs = lambda input: " ".join(input)
#     conda:
#         WORKING_DIR + "envs/deeptools.yaml"
#     shell:
#         "multiBigwigSummary bins -b {params.all_bigwigs} -o {output}"

rule phantompeakqualtools:
    input:
        BAM_DIR + "{exp}.trimmed.{bam_type}.bam"
    output:
        BAM_DIR + "{exp}.trimmed.{bam_type}.bam.qc.txt"
    conda:
        WORKING_DIR + "envs/phantompeakqualtools.yaml"
    shell:
        "run_spp.R -rf -c={input} -savp -out={input}.qc.txt"

rule macs2_narrowPeak_controlfree:
    input:
        BAM_DIR + "{sample}.trimmed.bwa_hg19.rmdup.bam"
    output:
        MACS2_DIR + "{sample}/{sample}_MACS2_nocontrol_peaks.narrowPeak"
    params:
        outdir = MACS2_DIR + "{sample}/",
        sample = "{sample}_MACS2_nocontrol"
    conda:
        WORKING_DIR + "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input} --name {params.sample} --outdir {params.outdir}"

rule macs2_broadPeak_controlfree:
    input:
        BAM_DIR + "{sample}.trimmed.bwa_hg19.rmdup.bam"
    output:
        MACS2_BROAD_DIR + "{sample}/{sample}_MACS2_nocontrol_broad_peaks.broadPeak"
    params:
        outdir = MACS2_BROAD_DIR + "{sample}/",
        sample = "{sample}_MACS2_nocontrol_broad"
    conda:
        WORKING_DIR + "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input} --name {params.sample} --outdir {params.outdir} --broad"

rule macs2_narrowPeak:
    input:
        treatment = BAM_DIR + "{sample}.trimmed.bwa_hg19.rmdup.bam",
        control = lambda wildcards: BAM_DIR + TREATMENT_TO_CONTROL[wildcards.sample] + ".trimmed.bwa_hg19.rmdup.bam"
    output:
        MACS2_DIR + "{sample}/{sample}_MACS2_peaks.narrowPeak"
    params:
        outdir = MACS2_DIR + "{sample}/",
        sample = "{sample}"
    conda:
        WORKING_DIR + "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input.treatment} --control {input.control} --name {params.sample}_MACS2 --outdir {params.outdir} && touch {output}"

rule macs2_broadPeak:
    input:
        treatment = BAM_DIR + "{sample}.trimmed.bwa_hg19.rmdup.bam",
        control = lambda wildcards: BAM_DIR + TREATMENT_TO_CONTROL[wildcards.sample] + ".trimmed.bwa_hg19.rmdup.bam"
    output:
        MACS2_BROAD_DIR + "{sample}/{sample}_MACS2_broad_peaks.broadPeak"
    params:
        outdir = MACS2_BROAD_DIR + "{sample}/",
        sample = "{sample}_MACS2_broad"
    conda:
        WORKING_DIR + "envs/macs2.yaml"
    shell:
        "macs2 callpeak --treatment {input.treatment} --control {input.control} --name {params.sample} --outdir {params.outdir} --broad"

rule hmcan_narrowPeak:
    input:
        treatment = BAM_DIR + "{sample}.trimmed.bwa_hg19.rmdup.bam",
        control = lambda wildcards: BAM_DIR + TREATMENT_TO_CONTROL[wildcards.sample] + ".trimmed.bwa_hg19.rmdup.bam"
    output:
        HMCAN_DIR + "{sample}/{sample}_hmcan_peaks.narrowPeak"
    params:
        hmcan_bin = HMCAN_BIN,
        config_file  = HMCAN_CONFIG,
        output_prefix = HMCAN_DIR + "{sample}/{sample}_hmcan"
    shell:
        "{params.hmcan_bin} {input.treatment} {input.control} {params.config_file} {params.output_prefix}"

rule LILY:
    input:
        HMCAN_DIR + "{sample}/{sample}_hmcan_peaks.narrowPeak"
    output:
        LILY_DIR + "{sample}_hmcan.scores.bed"
    params:
        hmcan_path = HMCAN_DIR + "{sample}/{sample}_hmcan",
        out_path = LILY_DIR,
        lily_script = LILY_SCRIPT,
        gene_annotations = LILY_GENEANNOTATIONS,
        fai  = LILY_FAI_FILE,
        stitching_distance = 12500,
        tss_distance = 2500
    conda:
        WORKING_DIR + "envs/lily.yaml"
    shell:
        "cat {params.lily_script} | R --slave --args {params.hmcan_path} {params.out_path} {params.stitching_distance} {params.tss_distance} {params.gene_annotations} {params.fai}"

rule peak_exclude_blacklisted:
    input:
        "{peakfile}Peak"
    output:
        "{peakfile}Peak.mappability_filtered.bed"
    params:
        blacklist = MAPPABILITY_BLACKLIST
    conda:
        WORKING_DIR + "envs/bedtools.yaml"
    shell:
        "bedtools intersect -a {input} -b {params.blacklist} -v > {output}"

rule multiqc:
    input:
        expand(FASTQ_DIR + "{exp}_fastqc.html", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.bam", exp=EXP),
        expand(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.bam.qc.txt", exp=EXP),
        expand(MACS2_DIR + "{treatment}/{treatment}_MACS2_peaks.narrowPeak", treatment=TREATMENTS)
    output:
        FASTQ_DIR + "multiqc_report.html"
    conda:
        WORKING_DIR + "envs/qc.yaml"
    shell:
        "multiqc -o " + FASTQ_DIR + " -f " + FASTQ_DIR + " " + BAM_DIR + " " + MACS2_DIR

rule heatmap_make_matrix_all_genes:
    input:
        BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.bw"
    output:
        temp(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.allgenes_deeptools.mat.gz")
    params:
        genes = "/fast/users/helmsauk_c/work/resources/rseqc-support/hg19_Ensembl_gene.bed",
        threads = "8"
    conda:
        WORKING_DIR + "envs/deeptools.yaml"
    shell:
        "computeMatrix scale-regions -S {input} -R {params.genes} --beforeRegionStartLength 3000 --regionBodyLength 5000 --afterRegionStartLength 3000 --skipZeros -o {output} -p {params.threads}"

rule heatmap_plot_all_genes:
    input:
        BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.allgenes_deeptools.mat.gz"
    output:
        BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.allgenes_deeptools.png"
    params:
        sample = "{exp}"
    conda:
        WORKING_DIR + "envs/deeptools.yaml"
    shell:
        "plotHeatmap -m {input} -out {output} --colorMap RdBu --samplesLabel {params.sample}"

rule heatmap_make_matrix_housekeeping:
    input:
        BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.bw"
    output:
        temp(BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.housekeeping_deeptools.mat.gz")
    params:
        housekeeping_genes = "/fast/users/helmsauk_c/work/resources/rseqc-support/hg19.HouseKeepingGenes.bed",
        threads = "8"
    conda:
        WORKING_DIR + "envs/deeptools.yaml"
    shell:
        "computeMatrix scale-regions -S {input} -R {params.housekeeping_genes} --beforeRegionStartLength 3000 --regionBodyLength 5000 --afterRegionStartLength 3000 --skipZeros -o {output} -p {params.threads}"

rule heatmap_plot_housekeeping:
    input:
        BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.housekeeping_deeptools.mat.gz"
    output:
        BAM_DIR + "{exp}.trimmed.bwa_hg19.rmdup.filtered.housekeeping_deeptools.png"
    params:
        sample = "{exp}"
    conda:
        WORKING_DIR + "envs/deeptools.yaml"
    shell:
        "plotHeatmap -m {input} -out {output} --colorMap RdBu --samplesLabel {params.sample}"

rule svaba:
    input:
        bam = BAM_DIR + "{sample}.trimmed.bwa_hg19.rmdup.bam"
    output:
        sv = SVABA_DIR + "{sample}.trimmed.bwa_hg19.rmdup.svaba.sv.vcf.gz",
        indel = SVABA_DIR + "{sample}.trimmed.bwa_hg19.rmdup.svaba.indel.vcf.gz"
    params:
        threads = 10,
        reference_genome_for_bwa = REFERENCE_PREINDEXED,
        exp = SVABA_DIR + "{sample}.trimmed.bwa_hg19.rmdup",
        blacklist_bed = "/fast/users/helmsauk_c/work/resources/svaba-support/svaba_exclusions.bed" # from https://data.broadinstitute.org/snowman/
    shell:
        "samtools index {input.bam} && " + SVABA_BIN_PATH + "svaba run -z -t {input.bam} -a {params.exp} -p {params.threads} -G {params.reference_genome_for_bwa} --germline --read-tracking --blacklist {params.blacklist_bed}"

# see https://github.com/taoliu/MACS/wiki/Call-differential-binding-events
# rule_diff_enrichment:
#     input:
#         cond1_treat_pileup = "cond1_treat_pileup.bdg",
#         cond1_control_lambda = "cond1_control_lambda.bdg",
#         cond2_treat_pileup = "cond2_treat_pileup.bdg",
#         cond2_control_lambda = "cond2_control_lambda.bdg"
#     output:
#         diff_c1_vs_c2_c3.0_cond1.bed
#     params:
#         output_prefix = diff_c1_vs_c2_c3
#     conda:
#         WORKING_DIR + "envs/macs2.yaml"
#     shell:
#         "macs2 bdgdiff --t1 {input.cond1_treat_pileup} --c1 {input.cond1_control_lambda} --t2 {input.cond2_treat_pileup}\
#    --c2 {input.cond2_control_lambda} --d1 12914669 --d2 14444786 -g 60 -l 120 --o-prefix {params.output_prefix}
